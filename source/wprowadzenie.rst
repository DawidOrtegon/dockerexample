Docker
================================

Docker is an open platform that is used to run virtual machines without having to install an operating system on the
machine where you want to run. It works by opening images that are inserted into containers, which can be executed
on the desired machine.

Docker Composer
---------------------------------
Docker compose is a tool for running and defining multiple type containers with the help of YAML files.
In this way it is easier for them to relate and connect with each other.
