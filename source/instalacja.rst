Build and initialize Docker.
======================================

1. Define the Dockerfile in the main project directory.
2. With "docker build -t lab3" build the image.
3. With "docker run -p 8090:8080 lab3" run the container of the previous image created.
4. To check the existing images "docker image ls -a".
5. To check the running containers "docker ps".
6. To stop the container "docker stop containerID".

Run the application with Docker Compose
-----------------------------------------
1. To make a new application a file named "docker-compose.yaml" has to be defined.
2. A file .env has to be defined with the information about the user, password and database port.
3. Build the image of the database with "docker-compose build".
4. Run the database with "docker-compose up db".
